<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmailSubscriber;

class EmailSubscriberController extends Controller
{
    public function create(Request $request){
        $email = $request->email;

        $sub = new EmailSubscriber;
        $sub->fill(["email"=>$email]);
        $sub->save();
        
        return redirect('/')->with('status', 'Thank you for subscribing, we\'ll notify you when we go live!');
    }
}
