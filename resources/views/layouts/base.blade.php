<!doctype html>
<html class="no-js" lang="en-US">
	<head>

		<!-- meta -->
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

		<!-- Uncomment the meta tags you are going to use! Be relevant and don't spam! -->

		<meta name="keywords" content="@yield('meta-keywords')" />
		<meta name="description" content="@yield('meta-title')">

		<!-- Title -->
		<title>AttentionFunnel | DIGITAL 1ST MARKETING</title>

		<!-- Url of your website (without extra pages) 
		<link rel="canonical" href="http://www.hogash-demos.com/kallyas_html/" />
		-->

		<!-- Restrict google from scanning info from Dmoz or YahooDir
		More here: http://www.seoboy.com/what-are-the-meta-tags-noodp-and-noydir-used-for-in-seo/
		Also more on robots here https://yoast.com/articles/robots-meta-tags/ 
		<meta name="robots" content="noodp,noydir"/>
		-->

		<!--
		Social media tags and more >>>>> http://moz.com/blog/meta-data-templates-123 <<<<<
		Debugging tools:
		- https://dev.twitter.com/docs/cards/validation/validator
		- https://developers.facebook.com/tools/debug
		- http://www.google.com/webmasters/tools/richsnippets
		- http://developers.pinterest.com/rich_pins/validator/
		-->

		<!-- Google Authorship and Publisher Markup. You can also simply add your name.
		Author = Owner, Publisher = who built the website. Add profile url in href="".
		Profile url example: https://plus.google.com/1130658794498306186 or replace [Google+_Profile] below with your profile # 
		<link rel="author" href="https://plus.google.com/[Google+_Profile]/posts"/>
		<link rel="publisher" href="https://plus.google.com/[Google+_Page_Profile]"/>
		-->

		<!-- Schema.org markup for Google+ 
		<meta itemprop="name" content="Kallyas Premium Template">
		<meta itemprop="description" content="This is the page description">
		<meta itemprop="image" content="">
		-->

		<!-- Open Graph Protocol meta tags.
		Used mostly for Facebook, more here http://ogp.me/ 
		<meta property="og:locale" content="en"/>
		<meta property="og:type" content="website"/>
		<meta property="og:title" content="Kallyas Premium Template"/>
		<meta property="og:description" content="Kallyas is an ultra-premium, responsive theme built for todays websites."/>
		<meta property="og:site_name" content="Kallyas Premium Template"/>
		-->

		<!-- Url of your website 
		<meta property="og:url" content=""/>
		-->

		<!-- Representative image 
		<meta property="og:image" content=""/>
		-->

		<!-- Twitter Cards
		Will generate a card based on the info below.
		More here: http://davidwalsh.name/twitter-cards or https://dev.twitter.com/docs/cards 
		<meta name="twitter:card" content="summary">
		-->

		<!-- Representative image 
		<meta name="twitter:image" content="">
		<meta name="twitter:domain" content="hogash.com">
		<meta name="twitter:site" content="@hogash">
		<meta name="twitter:creator" content="@hogash">
		-->

		<!-- Url of your website 
		<meta name="twitter:url" content="">
		<meta name="twitter:title" content="How to Create a Twitter Card">
		<meta name="twitter:description" content="Twitter's new Twitter Cards API allows developers to add META tags to their website, and Twitter will build card content from links to a given site.">
		-->

		<!-- GeoLocation Meta Tags / Geotagging. Used for custom results in Google.
		Generator here http://mygeoposition.com/ 
		<meta name="geo.placename" content="Chicago, IL, USA" />
		<meta name="geo.position" content="41.8781140;-87.6297980" />
		<meta name="geo.region" content="US-Illinois" />
		<meta name="ICBM" content="41.8781140, -87.6297980" />
		-->

		<!-- Dublin Core Metadata Element Set
		Using DC metadata is advantageous from an SEO perspective because search engines might interpret the extra code as an effort to make page content as descriptive and relevant as possible.

		<link rel="schema.DC" href="http://purl.org/DC/elements/1.0/" />
		<meta name="DC.Title" content="Kallyas Premium Template, Kallyas Responsive Template" />
		<meta name="DC.Creator" content="hogash" />
		<meta name="DC.Type" content="software" />
		<meta name="DC.Date" content="2015-10-01" />
		<meta name="DC.Format" content="text/html" />
		<meta name="DC.Language" content="en" />
		-->

		<!-- end descriptive meta tags -->

		<!-- Retina /images -->
		<!-- Simply uncomment to use this script !! More here http://retina-/images.complexcompulsions.com/
		<script>(function(w){var dpr=((w.devicePixelRatio===undefined)?1:w.devicePixelRatio);if(!!w.navigator.standalone){var r=new XMLHttpRequest();r.open('GET','/retina/images.php?devicePixelRatio='+dpr,false);r.send()}else{document.cookie='devicePixelRatio='+dpr+'; path=/'}})(window)</script>
		<noscript><style id="devicePixelRatio" media="only screen and (-moz-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)">html{background-image:url("php-helpers/_retina/images.php?devicePixelRatio=2")}</style></noscript>-->
		<!-- End Retina /images -->

		<!-- iDevices & Retina Favicons 
		<link rel="apple-touch-icon" href="/images/favicons/apple-touch-icon-144-precomposed.png" sizes="144x144">
		<link rel="apple-touch-icon" href="/images/favicons/apple-touch-icon-114-precomposed.png" sizes="114x114">
		<link rel="apple-touch-icon" href="/images/favicons/apple-touch-icon-72-precomposed.png" sizes="72x72">
		<link rel="apple-touch-icon" href="apple-touch-icon-57-precomposed.png" sizes="57x57">-->

		<!--  Desktop Favicons  -->
		<link rel="icon" type="image/png" href="/images/favicons/favicon-16x16.png" sizes="16x16">
		<!-- <link rel="icon" type="image/png" href="/images/favicons/favicon-32x32.png" sizes="32x32"> -->
		<!-- <link rel="icon" type="image/png" href="/images/favicons/favicon-96x96.png" sizes="96x96"> -->

		<!-- Google Fonts /css Stylesheet // More here http://www.google.com/fonts#UsePlace:use/Collection:Open+Sans -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
		<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all">
		<link rel="stylesheet" href="/css/sliders/ios/style.css" type="text/css" media="all">

		<!-- ***** Main + Responsive & Base sizing /css Stylesheet ***** -->
		<link rel="stylesheet" href="/css/template.css" type="text/css" media="all">
		<link rel="stylesheet" href="/css/responsive.css" type="text/css" media="all">
		<link rel="stylesheet" href="/css/base-sizing.css" type="text/css" media="all">

		<!-- Custom /css Stylesheet (where you should add your own /css rules) -->
		<link rel="stylesheet" href="/css/custom.css" type="text/css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.css"/>
		<!-- Modernizr Library -->
		<script type="text/javascript" src="/js/modernizr.min.js"></script>

		<!-- jQuery Library -->
		<script type="text/javascript" src="/js/jquery.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.js"></script>
		
	</head>

	<body class="@yield('body-class')">
		<!-- Page Wrapper -->
		<div id="page_wrapper">    
			@include('headers.header')
			@yield('content')
			@include('footers.footer')
		</div>
		
	</body>
	<script>
    AOS.init({
      offset: 200,
      duration: 600,
      easing: 'ease-in-sine',
	  delay: 100,
	  disable: 'mobile'
    });
  </script>
</html>