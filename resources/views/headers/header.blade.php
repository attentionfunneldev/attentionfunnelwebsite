<header id="header" class="site-header style5 new cta_button" data-header-style="7">
			<!-- siteheader-container -->
			<div class="container siteheader-container header--oldstyles">
				<!-- Logo container-->
				<div class="logo-container hasInfoCard logosize--yes">
					<!-- Logo -->
					<h1 class="site-logo logo " id="logo">
						<a href="/">
							<img src="/images/attention-funnel-logo.png" class="logo-img dark-logo" alt="Attention Funnel" title="Hacking Social Culture" />
						</a>
					</h1>
					<!--/ Logo -->
				</div>
				<!--/ Logo container-->

				<!-- separator for responsive header -->
				<div class="separator site-header-separator visible-xs"></div>
				<!--/ separator for responsive header -->

				<!-- header-links-container -->
				<div class="header-links-container">
					<!-- Header Social links -->
					<ul class="social-icons sc--clean topnav navRight">
						<li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
						<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>	
						<li><a href="#" target="_self" class="icon-pinterest" title="Pinterest"></a></li>
						<li><a href="#" target="_blank" class="icon-gplus" title="Google Plus"></a></li>
					</ul>	
					<!--/ Header Social links -->		

				<!-- Call to action ribbon Free Quote -->
				<a href="#" id="ctabutton" class="ctabutton kl-cta-ribbon" title="GET A FREE QUOTE" target="_self"><strong>FREE</strong>QUOTE<svg version="1.1" class="trisvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" preserveaspectratio="none" width="14px" height="5px" viewbox="0 0 14.017 5.006" enable-background="new 0 0 14.017 5.006" xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" d="M14.016,0L7.008,5.006L0,0H14.016z"></path></svg></a>
				<!--/ Call to action ribbon Free Quote -->

				<!-- responsive menu trigger -->
				<div id="zn-res-menuwrapper">
					<a href="#" class="zn-res-trigger zn-header-icon"></a>
				</div>
				<!--/ responsive menu trigger -->

				@include('partials.menu')
			</div>
			<!--/ siteheader-container -->
		</header>