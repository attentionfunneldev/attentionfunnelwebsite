@extends('layouts.base')

@section('body-class') services @endsection

@section('content')

    @section('subheader-background-image') /images/about-me.jpg @endsection 
    @section('page-title') SERVICES @endsection {{-- PROGRAMS--}}
    @section('subtitle') WHAT WE CAN DO FOR YOU @endsection
    @section('description') Explore what our team at AttentionFunnel can provide your business.
    @endsection
    
    @include('subheaders.subheader')
		<script>console.log('Hey Allan');</script>
		<section class="hg_section ptop-80 pbottom-20">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
							<!-- Title -->
							<h3 class="tbk__title montserrat fw-bold fs-28">WHO WE ARE AND WHAT WE DO</h3>
							<!--/ Title -->

							<!-- Sub-Title -->
							<h4 class="tbk__subtitle fw-vthin fs-18 lh-32">Dramatically expedite emerging ROI through diverse deliverables. Phosfluorescently integrate covalent portals without cooperative e-services. Energistically synergize business methods of empowerment with intuitive systems. Energistically extend accurate web-readiness without user-centric leadership skills.</h4>
							<!--/ Sub-Title -->
						</div>

						<!-- separator -->
						<div class="hg_separator clearfix mb-65">
						</div>
						<!--/ separator -->
					</div>
					<!--/ col-md-12 col-sm-12 -->

					<div class="col-md-6 col-sm-6">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default">
									<div class="kl-iconbox__inner">
										<div class="kl-iconbox__icon-wrapper ">
											<!-- Icon -->
											<img class="kl-iconbox__icon" src="images/ib-ico-12.svg" alt="WEB DESIGN SERVICES">
											<!--/ Icon -->
										</div>
										<!--/ .kl-iconbox__icon-wrapper -->

										<!-- content -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title">WEB DEVELOPMENT</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc">
													Using bleeding edge technology to bring your vision into reality with efficiency and ease.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!--/ .kl-iconbox__content-wrapper -->
									</div>
									<!--/ .kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
							<!--/ col-md-6 col-sm-6 -->

							<div class="col-md-6 col-sm-6">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default">
									<div class="kl-iconbox__inner">
										<div class="kl-iconbox__icon-wrapper">
											<!-- Icon -->
											<img class="kl-iconbox__icon" src="images/ib-ico-21.svg" alt="GRAPHIC DESIGN">
											<!--/ Icon -->
										</div>
										<!--/ .kl-iconbox__icon-wrapper -->

										<!-- content -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title">GRAPHIC DESIGN</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc">
													Commanding an excellent armada of designers that will blow your mind with their creativity.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!--/ .kl-iconbox__content-wrapper -->
									</div>
									<!--/ kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
							<!--/ col-md-6 col-sm-6 -->
						</div>
						<!--/ row -->

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default ">
									<div class="kl-iconbox__inner">
										<div class="kl-iconbox__icon-wrapper ">
											<!-- Icon -->
											<img class="kl-iconbox__icon" src="images/ib-ico-4.svg" alt="SEO SERVICES">
											<!--/ Icon -->
										</div>
										<!--/ .kl-iconbox__icon-wrapper -->

										<!-- content -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title">SEO SERVICES</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc">
													We identify exactly what your website needs to sit at the big kids table.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!--/ .kl-iconbox__content-wrapper -->
									</div>
									<!--/ .kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
							<div class="col-md-6 col-sm-6">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default ">
									<div class="kl-iconbox__inner">
										<div class="kl-iconbox__icon-wrapper">
											<!-- Icon -->
											<img class="kl-iconbox__icon" src="images/ib-ico-5.svg" alt="MARKETING">
											<!--/ Icon -->
										</div>
										<!--/ .kl-iconbox__icon-wrapper -->

										<!-- content -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title">MARKETING</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc">
													Hacking social culture to achieve the best ROI with the least amount of time and effort.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!--/ .kl-iconbox__content-wrapper -->
									</div>
									<!--/ .kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
						</div>
						<!--/ row -->
					</div>
					<!--/ col-md-6 col-sm-6 -->

					<div class="col-md-6 col-sm-6">
						<!-- spacer -->
						<div class="th-spacer clearfix" style="height: 60px;">
						</div>
						<!--/ spacer -->

						<!-- Skills Diagram -->
						<div id="skills_diagram_el" class="kl-skills-diagram">
							<div class="kl-skills-legend legend-topright">
								<h4>LEGEND</h4>
								<ul class="kl-skills-list">
									<li data-percent="80" style="background-color:#97be0d;">Javascript</li>
									<li data-percent="90" style="background-color:#d84f5f;">SEO/SEM</li>
									<li data-percent="77" style="background-color:#6eabe5;">Wordpress</li>
									<li data-percent="64" style="background-color:#8dc9e8;">Design</li>
									<li data-percent="65" style="background-color:#bdc3c7;">Swift </li>
								</ul>
							</div>
							<div class="skills-responsive-diagram">
								<div id="thediagram" class="kl-diagram" data-width="600" data-height="600" data-maincolor="#193340" data-maintext="Skills" data-fontsize="20px Arial" data-textcolor="#ffffff"></div>
							</div>
						</div>
						<!-- Required scripts for Skills Diagram -->
						<script type="text/javascript" src="js/plugins/raphael_diagram/raphael-min.js"></script>
						<script type="text/javascript" src="js/plugins/raphael_diagram/init.js"></script>
						<!--/ Skills Diagram -->
					</div>
					<!--/ col-md-6 col-sm-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Title + Services + Skills Diagram -->
@endsection