@extends('layouts.base')

@section('content')
    
    @section('subheader-background-image') /images/about-me.jpg @endsection
    @section('page-title') NEWS @endsection
    @section('subtitle') HACKING SOCIAL CULTURE @endsection
    @section('description') At AttentionFunnel we leverage social culture to elevate your brand and online presence. 
                                        Join us as we guide you into the forefront of the digital frontier.
    @endsection
    
    @include('subheaders.subheader')
    
@endsection