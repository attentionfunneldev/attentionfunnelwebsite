<!doctype html>
<html class="no-js" lang="en-US">
<head>

	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

	

	<meta name="keywords" content="premium html template, unique premium template, multipurpose template" />
	<meta name="description" content="Kallyas is an ultra-premium, responsive theme built for todays websites. Create your website, fast.">
    
    <title>Attention Funnel | Coming Soon Page</title>


	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/template.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/base-sizing.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<script type="text/javascript" src="js/modernizr.min.js"></script>

	<!-- jQuery Library -->
	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us16.list-manage.com","uuid":"06ab0e1ef4dbf1629f5a6b4d3","lid":"4d37d4c826"}) })</script>
</head>

<body class="offline-page">


	<!-- Page Wrapper -->
	<div id="page_wrapper">
		<div class="containerbox">
			<!-- Logo container -->
			<div class="containerbox__logo">
				<h3 class="site-logo logo" id='logo'>
					<a href='/'>
						<img class="logo-img" src="/images/attention-funnel-logo.png" alt="Attention Funnel Logo" title="Hacking Social Culture"/>
					</a>
				</h3>
			</div>
			<!--/ Logo container -->

			<!-- Content -->
			<div class="content">
				<p><strong>Pay Attention. We are Launching Soon.</strong><br/> Send us your email and we will keep you updated with what’s going on in our world.</p>

				<!-- Counter -->
				<div class="ud_counter">
					<ul class="sc_counter hasCountdown">
						<li>
							<span class="days"></span>
							<p class="timeRefDays"></p>
						</li>
						<li>
							<span class="hours"></span>
							<p class="timeRefHours"></p>
						</li>
						<li>
							<span class="minutes"></span>
							<p class="timeRefMinutes"></p>
						</li>
						<li>
							<span class="seconds"></span>
							<p class="timeRefSeconds"></p>
						</li>
					</ul>
					<span class="till_lauch"><img src="images/rocket.png"></span>
				</div>
				<!--/ Counter -->
                <div class="newsletter-signup">
                    <form action="/subscribe" method="POST">
                        {{csrf_field()}}
                        <input class="nl-email form-control" name="email" type="email" placeholder="your.email@email.com" required />
                        <input class="nl-submit" type="submit" value="JOIN US" />
                    </form>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
				<!-- Social icons -->
				<ul class="social-icons sc--normal clearfix">
					<li><a href="https://www.facebook.com/AttentionFunnel/" target="_blank" title="Facebook" class="icon-facebook"></a></li>
					<li><a href="https://twitter.com/attentionfunnel" target="_blank" title="Twitter" class="icon-twitter"></a></li>
				</ul>
				<!--/ Social icons -->
				<div class="clear">
				</div>
			</div>
			<!-- end content -->
			<div class="clear">
			</div>
		</div>
		<!--/ .containerbox -->
	</div>
	<!--/ Page Wrapper -->


	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/kl-plugins.js"></script>

	<!-- JS FILES // Loaded on this page -->
	<!-- Required countdown coming soon -->
	<script type="text/javascript" src="js/plugins/event-countdown-coming-soon.js"></script>

	<!-- Custom Kallyas JS codes -->
	<script type="text/javascript" src="js/kl-scripts.js"></script>

	<!-- Custom user JS codes -->
	<script type="text/javascript" src="js/kl-custom.js"></script>
	
	

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-XXXXX-X', 'auto');
	  ga('send', 'pageview');
	</script>
	-->

</body>
</html>