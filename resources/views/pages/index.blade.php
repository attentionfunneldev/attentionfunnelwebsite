@extends('layouts.base')

@section('content')
	<!-- iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->
    <div class="kl-slideshow iosslider-slideshow uh_light_gray maskcontainer--shadow_ud iosslider--custom-height scrollme">
			<!-- Loader -->
			<div class="kl-loader">
				<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path><path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z" transform="rotate(98.3774 20 20)"><animatetransform attributetype="xml" attributename="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatcount="indefinite"></animatetransform></path></svg>
			</div>
			<!-- Loader -->

			<div class="bgback">
			</div>

			<!-- Animated Sparkles -->
			<div class="th-sparkles">
			</div>
			<!--/ Animated Sparkles -->

			<!-- iOS Slider wrapper with animateme scroll efect -->
			<div class="iosSlider kl-slideshow-inner animateme" data-trans="6000" data-autoplay="1" data-infinite="true" data-when="span" data-from="0" data-to="0.75" data-translatey="300" data-easing="linear">
				<!-- Slides -->
				<div class="kl-iosslider hideControls">
					<!-- Slide 1 -->
					<div class="item iosslider__item">
						<!-- Image -->
						<div class="slide-item-bg" style="background-image:url(images/sliders/ios02-lq.jpg);">
						</div>
						<!--/ Image -->

						<!-- Gradient overlay -->
						<div class="kl-slide-overlay" style="background:rgba(32,55,152,0.4); background: -moz-linear-gradient(left, rgba(32,55,152,0.4) 0%, rgba(17,93,131,0.25) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(32,55,152,0.4)), color-stop(100%,rgba(17,93,131,0.25))); background: -webkit-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -o-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -ms-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: linear-gradient(to right, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); ">
						</div>
						<!--/ Gradient overlay -->

						<!-- Captions container -->
						<div class="container kl-iosslide-caption kl-ioscaption--style4 s4ext fromleft klios-alignleft kl-caption-posv-middle">
							<!-- Captions animateme wrapper -->
							<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
								<!-- Main Big Title -->
								<h2 class="main_title has_titlebig "><span><strong>SUMMER SALE</strong> STEAM</span></h2>
								<!--/ Main Big Title -->

								<!-- Big Title -->
								<h3 class="title_big">UP TO <strong>40% OFF</strong></h3>
								<!--/ Big Title -->

								<!-- Link button -->
								<a class="more " href="#" target="_self">SEE COLLECTION</a>
								<!--/ Link button -->

								<!-- Small Title -->
								<h4 class="title_small">FOR NEW ARRIVALS OF 2017'S COLLECTION</h4>
								<!--/ Small Title -->
							</div>
							<!--/ Captions animateme wrapper -->
						</div>
						<!--/ Captions container -->
					</div>
					<!--/ Slide 1 -->

					<!-- Slide 2 -->
					<div class="item iosslider__item">
						<!-- Image -->
						<div class="slide-item-bg" style="background-image:url(images/sliders/216h-lq.jpg);">
						</div>
						<!--/ Image -->

						<!-- Color overlay -->
						<div class="kl-slide-overlay" style="background-color:rgba(6,111,217,0.3)">
						</div>
						<!--/ Color overlay -->

						<!-- Captions container -->
						<div class="container kl-iosslide-caption kl-ioscaption--style4 fromright klios-alignright kl-caption-posv-middle">
							<!-- Captions animateme wrapper -->
							<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
								<!-- Main Big Title -->
								<h2 class="main_title has_titlebig "><span><strong>HUGE</strong> SUMMER SALE</span></h2>
								<!--/ Main Big Title -->

								<!-- Big Title -->
								<h3 class="title_big"><span style="ff-alternative">ALL UNDER $49</span></h3>
								<!--/ Big Title -->

								<!-- Link button -->
								<a class="more " href="#" target="_self">SHOP NOW</a>
								<!--/ Link button -->

								<!-- Small Title -->
								<h4 class="title_small">NEW OUTFITS FOR YOUR SUMMER'S WARDROBE</h4>
								<!--/ Small Title -->
							</div>
							<!--/ Captions animateme wrapper -->
						</div>
						<!--/ Captions container -->
					</div>
					<!--/ Slide 2 -->

					<!-- Slide 3 -->
					<div class="item iosslider__item">
						<!-- Video background container -->
						<div class="kl-video-container">
							<!-- Video wrapper -->
							<div class="kl-video-wrapper video-grid-overlay">
								<!-- Self Hosted Video Source -->
								<div class="kl-video valign halign" style="width: 100%; height: 100%;" data-setup='{ 
									"position": "absolute", 
									"loop": true, 
									"autoplay": true, 
									"muted": true, 
									"mp4": "videos/Working-Space.mp4", 
									"poster": "videos/Working-Space.jpg", 
									"video_ratio": "1.7778" }'>
								</div>
								<!--/ Self Hosted Video Source -->
							</div>
							<!--/ Video wrapper -->
						</div>
						<!--/ Video background container -->

						<!-- Color overlay -->
						<div class="kl-slide-overlay" style="background-color:rgba(4,43,135,0.45)">
						</div>
						<!-- Color overlay -->

						<!-- Captions container -->
						<div class="container kl-iosslide-caption kl-ioscaption--style5 fromleft klios-alignleft kl-caption-posv-middle">
							<!-- Captions animateme wrapper -->
							<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
								<!-- Main Big Title -->
								<h2 class="main_title has_titlebig "><span>UNIQUE • RAFINEMENT<br>
								FULL COMFORT</span></h2>
								<!--/ Main Big Title -->

								<!-- Big Title -->
								<h3 class="title_big">DISCOUNTS <strong>UP TO 50%</strong></h3>
								<!--/ Big Title -->

								<!-- Link buttons -->
								<div class="more">
									<!-- Button full color style -->
									<a class="btn btn-fullcolor " href="#" target="_self">SEE COLLECTION</a>
									<!--/ Button full color style -->

									<!-- Button full lined style -->
									<a class="btn btn-lined " href="#" target="_self">START SHOPPING</a>
									<!--/ Button full lined style -->
								</div>
								<!--/ Link buttons -->

								<!-- Small Title -->
								<h4 class="title_small">FOR NEW ARRIVALS OF 2017'S COLLECTION</h4>
								<!--/ Small Title -->
							</div>
							<!--/ Captions animateme wrapper -->
						</div>
						<!--/ Captions container -->

						<!-- Image boxes -->
						<div class="klios-imageboxes fromleft klios-alignleft middle ">
							<!-- Image boxes wrapper -->
							<div class="kl-imgbox-inner">
								<!-- Box #1 -->
								<div class="kl-imgbox kl-imgbox--1">
									<a href="#" title="" class="kl-imgbox--link" style="background-image:url(images/sliders/Banner-4.png)"></a>
								</div>
								<!--/ Box #1 -->

								<!-- Box #2 -->
								<div class="kl-imgbox kl-imgbox--2">
									<a href="#" title="" class="kl-imgbox--link" style="background-image:url(images/sliders/Banner-3.png)"></a>
								</div>
								<!--/ Box #2 -->

								<!-- Box #3 -->
								<div class="kl-imgbox kl-imgbox--3">
									<a href="#" title="" class="kl-imgbox--link" style="background-image:url(images/sliders/Banner-1.png)"></a>
								</div>
								<!--/ Box #3 -->
							</div>
							<!--/ Image boxes wrapper -->
						</div>
						<!--/ Image boxes -->
					</div>
					<!--/ Slide 3 -->

					<!-- Slide 4 -->
					<div class="item iosslider__item">
						<!-- Image -->
						<div class="slide-item-bg" style="background-image:url(images/sliders/ios-living.jpg);">
						</div>
						<!--/ Image -->

						<!-- Gradient overlay -->
						<div class="kl-slide-overlay" style="background:rgba(91,48,0,0.3); background: -moz-linear-gradient(left, rgba(91,48,0,0.3) 0%, rgba(53,53,53,0.25) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(91,48,0,0.3)), color-stop(100%,rgba(53,53,53,0.25))); background: -webkit-linear-gradient(left, rgba(91,48,0,0.3) 0%,rgba(53,53,53,0.25) 100%); background: -o-linear-gradient(left, rgba(91,48,0,0.3) 0%,rgba(53,53,53,0.25) 100%); background: -ms-linear-gradient(left, rgba(91,48,0,0.3) 0%,rgba(53,53,53,0.25) 100%); background: linear-gradient(to right, rgba(91,48,0,0.3) 0%,rgba(53,53,53,0.25) 100%); ">
						</div>
						<!--/ Gradient overlay -->

						<!-- Captions container -->
						<div class="container kl-iosslide-caption kl-ioscaption--style5 zoomin klios-aligncenter kl-caption-posv-middle">
							<!-- Captions animateme wrapper -->
							<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
								<!-- Main Big Title -->
								<h2 class="main_title has_titlebig kl-ios-has-sqbox "><span class="kl-ios-sqbox"></span><span><strong>YOUR LIVING</strong><br>
								DESERVES BETTER</span></h2>
								<!--/ Main Big Title -->

								<!-- Big Title -->
								<h3 class="title_big"><strong>INNOVATION AND PERFORMANCE </strong><br>
								SINCE 1978</h3>
								<!--/ Big Title -->

								<!-- Link buttons -->
								<div class="more">
									<!-- Button full color style -->
									<a class="btn btn-fullcolor " href="#" target="_self">SEE OUR LATEST OFFERS</a>
									<!--/ Button full color style -->
								</div>
								<!--/ Link buttons -->
							</div>
							<!--/ Captions animateme wrapper -->
						</div>
						<!--/ Captions container -->
					</div>
					<!--/ Slide 4 -->

					<!-- Slide 5 -->
					<div class="item iosslider__item">
						<!-- Image -->
						<div class="slide-item-bg" style="background-image:url(images/sliders/iosmain.jpg);">
						</div>
						<!--/ Image -->

						<!-- Gradient overlay -->
						<div class="kl-slide-overlay" style="background:rgba(132,30,193,0.25); background: -moz-linear-gradient(left, rgba(132,30,193,0.25) 0%, rgba(61,15,15,0.3) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(132,30,193,0.25)), color-stop(100%,rgba(61,15,15,0.3))); background: -webkit-linear-gradient(left, rgba(132,30,193,0.25) 0%,rgba(61,15,15,0.3) 100%); background: -o-linear-gradient(left, rgba(132,30,193,0.25) 0%,rgba(61,15,15,0.3) 100%); background: -ms-linear-gradient(left, rgba(132,30,193,0.25) 0%,rgba(61,15,15,0.3) 100%); background: linear-gradient(to right, rgba(132,30,193,0.25) 0%,rgba(61,15,15,0.3) 100%); ">
						</div>
						<!--/ Gradient overlay -->

						<!-- Captions container -->
						<div class="container kl-iosslide-caption kl-ioscaption--style6 fromleft klios-aligncenter kl-caption-posv-middle">
							<!-- Captions animateme wrapper -->
							<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
								<!-- Main Big Title -->
								<h2 class="main_title has_titlebig "><span><strong>SUMMER SALE</strong> STEAM</span></h2>
								<!-- Main Big Title -->

								<!-- Small Title -->
								<h4 class="title_small">FOR THE NEW ARRIVALS OF 2017'S COLLECTION</h4>
								<!--/ Small Title -->

								<!-- Video pop-up -->
								<div class="klios-playvid">
									<!-- Video link - youtube -->
									<a href="http://www.youtube.com/watch?v=JaAWdljhD5o?loop=1&amp;start=0&amp;autoplay=1&amp;controls=0&amp;showinfo=0&amp;wmode=transparent&amp;iv_load_policy=3&amp;modestbranding=1&amp;rel=0" data-lightbox="youtube">
									<!--/ Video link - youtube -->

									<!-- Icon = .glyphicon-play circled icon large size -->
									<i class="kl-icon glyphicon glyphicon-play circled-icon ci-large"></i>
								</a>
								</div>
								<!--/ Video pop-up -->
							</div>
							<!--/ Captions animateme wrapper -->
						</div>
						<!--/ Captions container -->
					</div>
					<!--/ Slide 5 -->
				</div>
				<!--/ Slides -->

				<!-- Navigation Controls - Prev -->
				<div class="kl-iosslider-prev">
					<!-- Arrow -->
					<span class="thin-arrows ta__prev"></span>
					<!--/ Arrow -->

					<!-- Label - prev -->
					<div class="btn-label">
						PREV
					</div>
					<!--/ Label - prev -->
				</div>
				<!--/ Navigation Controls - Prev -->

				<!-- Navigation Controls - Next -->
				<div class="kl-iosslider-next">
					<!-- Arrow -->
					<span class="thin-arrows ta__next"></span>
					<!--/ Arrow -->

					<!-- Label - next -->
					<div class="btn-label">
						NEXT
					</div>
					<!--/ Label - next -->
				</div>
				<!--/ Navigation Controls - Prev -->
			</div>
			<!--/ iOS Slider wrapper with animateme scroll efect -->

			<!-- Bullets -->
			<div class="kl-ios-selectors-block bullets2">
				<div class="selectors">
					<!-- Item #1 -->
					<div class="item iosslider__bull-item first">
					</div>
					<!--/ Item #1 -->

					<!-- Item #2 -->
					<div class="item iosslider__bull-item">
					</div>
					<!--/ Item #2 -->

					<!-- Item #3 -->
					<div class="item iosslider__bull-item">
					</div>
					<!--/ Item #3 -->

					<!-- Item #4 -->
					<div class="item iosslider__bull-item selected">
					</div>
					<!--/ Item #4 -->

					<!-- Item #5 -->
					<div class="item iosslider__bull-item">
					</div>
					<!--/ Item #5 -->
				</div>
				<!--/ .selectors -->
			</div>
			<!--/ Bullets -->

			<div class="scrollbarContainer">
			</div>

			<!-- Bottom mask style 2 -->
			<div class="kl-bottommask kl-bottommask--shadow_ud">
			</div>
			<!--/ Bottom mask style 2 -->
		</div>
		<!--/ iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->

		<!-- Action Box - Style 3 section with custom top padding and white background color -->
		<section class="hg_section bg-white ptop-0">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
							<div class="action_box_inner">
								<div class="action_box_content">
									<div class="ac-content-text">
										<!-- Title -->
										<h4 class="text"><span class="fw-thin">KALLYAS TEMPLATE, THE <span class="fw-semibold">SWISS ARMY SECRET</span> FOR BUILDING THE MODERN WEBSITE</span></h4>
										<!--/ Title -->

										<!-- Sub-Title -->
										<h5 class="ac-subtitle">Packed with all the goodies you can get, Kallyas is our flagship WordPress Theme</h5>
										<!--/ Sub-Title -->
									</div>

									<!-- Call to Action buttons -->
									<div class="ac-buttons">
										<a class="btn btn-lined ac-btn" href="#" target="_blank">JOIN OUR NEWSLETTER</a><a class="btn btn-fullwhite ac-btn" href="#" target="_blank">LEARN MORE</a>
									</div>
									<!--/ Call to Action buttons -->
								</div>
								<!--/ action_box_content -->
							</div>
							<!--/ action_box_inner -->
						</div>
						<!--/ action_box style3 -->
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Action Box - Style 3 section with custom top padding and white background color -->

		<!-- Title - Style 1 section with custom top padding -->
		<section class="hg_section bg-white ptop-65">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Title element -->
						<div class="kl-title-block clearfix text-center tbk-symbol-- tbk-icon-pos--after-title">
							<!-- Title with montserrat font, custom font size and line height, bold style and light gray color -->
							<h3 class="tbk__title montserrat fs-44 lh-44 fw-bold light-gray3">OK, SO WHY ATTENTION FUNNEL?</h3>
							<!--/ Title with montserrat font, custom font size and line height, bold style and light gray color -->

							<!-- Sub-Title with custom font size an very thin style -->
							<h4 class="tbk__subtitle fs-18 fw-vthin">Well, lots of reasons, but most importantly because..</h4>
							<!--/ Sub-Title with custom font size an very thin style -->
						</div>
						<!--/ Title element -->
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Title - Style 1 section with custom top padding -->

		<!-- Icon Box - Left Floated Style section -->
		<section class="hg_section bg-white">
			<div class="full_width">
				<div class="row">
					<div class="col-sm-offset-1 col-md-10 col-sm-10">
						<div class="row gutter-md">
							<div class="col-md-3 col-sm-3" data-aos="fade-up" data-aos-anchor-placement="top-center">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left" >
									<div class="kl-iconbox__inner">
										<!-- Icon -->
										<div class="kl-iconbox__icon-wrapper">
											<img class="kl-iconbox__icon" src="images/set-03-01.svg" alt="Stunning Page Builder">
										</div>
										<!--/ Icon -->

										<!-- /.kl-iconbox__icon-wrapper -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">Stunning Elements</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc fs-14 gray">
													Set up pages and content like a <strong>PRO</strong>. Coding is not required and a handy documentation is included.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!-- /.kl-iconbox__content-wrapper -->
									</div>
									<!--/ kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
							<!--/ col-md-3 col-sm-3 -->

							<div class="col-md-3 col-sm-3" data-aos-delay="200" data-aos="fade-up" data-aos-anchor-placement="top-center">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left">
									<div class="kl-iconbox__inner">
										<!-- Icon -->
										<div class="kl-iconbox__icon-wrapper">
											<img class="kl-iconbox__icon" src="images/set-03-02.svg" alt="Iconic Awarded Design">
										</div>
										<!--/ Icon -->

										<!-- /.kl-iconbox__icon-wrapper -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">Iconic Awarded Design</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc fs-14 gray">
													Our design is featured across multiple marketplaces and awarded for its looks. Walk-through and enjoy the visuals.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!-- /.kl-iconbox__content-wrapper -->
									</div>
									<!--/ kl-iconbox__inner -->
								</div>
								<!-- Icon box float left -->
							</div>
							<!--/ col-md-3 col-sm-3 -->

							<div class="col-md-3 col-sm-3" data-aos-delay="400" data-aos="fade-up" data-aos-anchor-placement="top-center">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left">
									<div class="kl-iconbox__inner">
										<!-- Icon -->
										<div class="kl-iconbox__icon-wrapper">
											<img class="kl-iconbox__icon" src="images/set-03-03.svg" alt="Featurewise Complete">
										</div>
										<!--/ Icon -->

										<!-- /.kl-iconbox__icon-wrapper -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">Featurewise Complete</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc fs-14 gray">
													Without a doubt, Kallyas is one of the most complete WordPress theme on the market, being packed with all the goodies and sweet gems.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!-- /.kl-iconbox__content-wrapper -->
									</div>
									<!--/ kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
							<!--/ col-md-3 col-sm-3 -->

							<div class="col-md-3 col-sm-3" data-aos-delay="600" data-aos="fade-up" data-aos-anchor-placement="top-center">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left">
									<div class="kl-iconbox__inner">
										<!-- Icon -->
										<div class="kl-iconbox__icon-wrapper ">
											<img class="kl-iconbox__icon" src="images/set-03-04.svg" alt="Mature project">
										</div>
										<!--/ Icon -->

										<!-- /.kl-iconbox__icon-wrapper -->
										<div class="kl-iconbox__content-wrapper">
											<!-- Title -->
											<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
												<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">Mature project</h3>
											</div>
											<!--/ Title -->

											<!-- Description -->
											<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
												<p class="kl-iconbox__desc fs-14 gray">
													In time, gathering awesome feedback from our loyal customers, Kallyas became a mature, stable and future-proof project.
												</p>
											</div>
											<!--/ Description -->
										</div>
										<!-- /.kl-iconbox__content-wrapper -->
									</div>
									<!--/ kl-iconbox__inner -->
								</div>
							</div>
							<!--/ col-md-3 col-sm-3 -->
						</div>
						<!--/ row gutter-md -->
					</div>
					<!--/ col-sm-offset-1 col-md-10 col-sm-10 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ full_width -->
		</section>
		<!--/ Icon Box - Left Floated Style section -->

		<!-- Image Boxes - Style 4 new section -->
		<section class="hg_section bg-white">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4" data-aos-delay="200" data-aos="zoom-out-right">
						<!--/ Image Box style 4 - left symbol style -->
						<div class="box image-boxes imgboxes_style4 kl-title_style_left">
							<!-- Image Box anchor link -->
							<a class="imgboxes4_link imgboxes-wrapper" href="#" target="_blank">
								<!-- Image -->
								<img src="images/imageboxes-set1-011-460x307.jpg" alt="RESCUE SUPPORT" title="RESCUE SUPPORT" class="img-responsive imgbox_image cover-fit-img" />
								<!--/ Image -->

								<span class="imgboxes-border-helper"></span>

								<!-- Title -->
								<h3 class="m_title imgboxes-title">RESCUE SUPPORT</h3>
								<!--/ Title -->
							</a>
							<!--/ Image Box anchor link -->

							<!-- Description -->
							<p>Our support team will do its best to provide the best possible and helpful answer for the issues you’re having.</p>
							<!--/ Description -->
						</div>
						<!--/ Image Box style 4 - left symbol style -->
					</div>
					<!--/ col-md-4 col-sm-4  -->

					<div class="col-md-4 col-sm-4"  data-aos-delay="400" data-aos="zoom-out-up">
						<!-- Image Box style 4 - left symbol style -->
						<div class="box image-boxes imgboxes_style4 kl-title_style_left">
							<!-- Image Box anchor link -->
							<a class="imgboxes4_link imgboxes-wrapper" href="#" target="_blank">
								<!-- Image -->
								<img src="images/imageboxes-set1-021-460x307.jpg" alt="E-COMMERCE READY" title="E-COMMERCE READY" class="img-responsive imgbox_image cover-fit-img" />
								<!--/ Image -->

								<span class="imgboxes-border-helper"></span>

								<!-- Title -->
								<h3 class="m_title imgboxes-title">E-COMMERCE READY</h3>
								<!--/ Title -->
							</a>
							<!-- Image Box anchor link -->

							<!-- Description -->
							<p>Build an online store blazing fast with WooCommerce and Kallyas's ready-made ecommerce functionalities.</p>
							<!-- Description -->
						</div>
						<!--/ Image Box style 4 - left symbol style -->
					</div>
					<!--/ col-md-4 col-sm-4  -->

					<div class="col-md-4 col-sm-4"  data-aos-delay="600" data-aos="zoom-out-left">
						<!-- Image Box style 4 - bottom border style-->
						<div class="box image-boxes imgboxes_style4 kl-title_style_bottom">
							<!-- Image Box anchor link -->
							<a class="imgboxes4_link imgboxes-wrapper" href="#" target="_blank">
								<!-- Image -->
								<img src="images/gpg4-640x426.jpg" alt="MULTI-LANGUAGE READY" title="MULTI-LANGUAGE READY" class="img-responsive imgbox_image cover-fit-img">
								<!--/ Image -->

								<span class="imgboxes-border-helper"></span>

								<!-- Title -->
								<h3 class="m_title imgboxes-title">MULTI-LANGUAGE READY</h3>
								<!--/ Title -->
							</a>
							<!--/ Image Box anchor link -->

							<!-- Description -->
							<p>Add as many language packs as you want, to showcase your website across the entire globe.</p>
							<!--/ Description -->
						</div>
						<!--/ Image Box style 4 - bottom border style-->
					</div>
					<!--/ col-md-4 col-sm-4  -->
				</div>
				<!--/ row -->
			</div>
			<!--/ hg_section_size container -->
		</section>
		<!--/ Image Boxes - Style 4 new section -->

		<!-- Screenshot Box section with custom paddings -->
		<section class="hg_section ptop-100 pbottom-100">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Screenshot Box -->
						<div class="screenshot-box kl-style-2 fixclear">
							<div class="row">
								<div class="col-sm-12 col-md-6">
									<!-- left-side -->
									<div class="left-side">
										<!-- Title -->
										<h3 class="title"><span class="fw-thin">This is Kallyas Template, a rich featured, epic, <span class="fw-semibold">mature and premium work.</span></span></h3>
										<!--/ Title -->

										<!-- features -->
										<ul class="features">
											<li>
												<h4>FRONTEND PAGE BUILDER</h4>
												<span>Comfortable and intuitive visual drag and drop builder.</span></li>
											<li>
												<h4>UNLIMITED COLORS</h4>
												<span>Personalize the theme’s colors as much as you want for almost any element.</span></li>
											<li>
												<h4>HIGHLY CUSTOMIZABLE</h4>
												<span>Yet highly intuitive and easy to use, in just a matter of hours become a pro.</span>
											</li>
										</ul>
										<!--/ features -->

										<!-- Call to Action button -->
										<a href="#" target="_blank" class="btn btn-fullcolor btn-third">BUY NOW</a>
										<!--/ Call to Action button -->
									</div>
									<!--/ left-side -->
								</div>
								<div class="col-sm-12 col-md-6">
									<!-- thescreenshot image carousel -->
									<div class="thescreenshot">
										<!-- controls -->
										<div class="controls"><a href="#" class="prev"></a><a href="#" class="next"></a></div>
										<!--/ controls -->

										<!-- screenshot-carousel slides -->
										<ul class="screenshot-carousel" data-carousel-pagination=".sc-pagination">
											<!-- Slide 1 -->
											<li>
												<img src="images/office-820390_640-580x380.jpg" width="580" height="380" alt="" />
											</li>
											<!--/ Slide 1 -->

											<!-- Slide 2-->
											<li>
												<img src="images/ipad-605439_640-580x380.jpg" width="580" height="380" alt="" />
											</li>
											<!--/ Slide 2 -->

											<!-- Slide 3 -->
											<li>
												<img src="images/phone-690091_640-580x380.jpg" width="580" height="380" alt="" />
											</li>
											<!--/ Slide 3 -->
										</ul>
										<!--/ screenshot-carousel slides -->

										<!-- pagination -->
										<div class="sc-pagination"></div>
										<!--/ pagination -->
									</div>
									<!-- thescreenshot image carousel -->
								</div>
							</div>
						</div>
						<!--/ Screenshot Box -->
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Screenshot Box section with custom paddings -->

		<!-- Media Container - Border Animate Style 2 section with background white color -->
		<section class="hg_section bg-white p-0">
			<div class="full_width">
				<div class="row gutter-lg">
					<div class="col-md-5 col-sm-12">
						<!-- Media container style 2 element - with custom height(.h-615) -->
						<div class="media-container style2 h-615">
							<!-- Background -->
							<div class="kl-bg-source">
								<!-- Background image -->
								<div class="kl-bg-source__bgimage" style="background-image:url(images/home-brochure.jpg); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:top; background-size:cover">
								</div>
								<!--/ Background image -->

								<!-- Gradient overlay -->
								<div class="kl-bg-source__overlay" style="background:rgba(137,173,178,0.3); background: -moz-linear-gradient(left, rgba(137,173,178,0.3) 0%, rgba(53,53,53,0.65) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(137,173,178,0.3)), color-stop(100%,rgba(53,53,53,0.65))); background: -webkit-linear-gradient(left, rgba(137,173,178,0.3) 0%,rgba(53,53,53,0.65) 100%); background: -o-linear-gradient(left, rgba(137,173,178,0.3) 0%,rgba(53,53,53,0.65) 100%); background: -ms-linear-gradient(left, rgba(137,173,178,0.3) 0%,rgba(53,53,53,0.65) 100%); background: linear-gradient(to right, rgba(137,173,178,0.3) 0%,rgba(53,53,53,0.65) 100%); ">
								</div>
								<!--/ Gradient overlay -->
							</div>
							<!--/ Background -->

							<!-- media container link button -->
							<a class="media-container__link media-container__link--btn media-container__link--style-borderanim2 " href="https://www.youtube.com/watch?v=cVt-3vbENOQ" data-lightbox="iframe">
								<!-- SVG border -->
								<div class="borderanim2-svg">
									<!-- svg -->
									<svg height="70" width="400" xmlns="http://www.w3.org/2000/svg">
										<rect class="borderanim2-svg__shape" height="70" width="400"></rect>
									</svg>
									<!--/ svg -->

									<!-- Title text -->
									<span class="media-container__text">KALLYAS THEME</span>
									<!--/ Title text -->
								</div>
								<!--/ SVG border -->
							</a>
							<!--/ media container link button -->
						</div>
						<!--/ media-container style2 h-615 -->
					</div>
					<!--/ col-md-5 col-sm-12 -->

					<div class="col-md-7 col-sm-12">
						<div class="custom_container p-5">
							<div class="row hg_col_eq_last">
								<div class="col-md-12 col-sm-12">
									<!-- Title element -->
									<div class="kl-title-block clearfix text-left tbk-symbol--line tbk-icon-pos--after-title pbottom-0">
										<!-- Title with montserrat font, custom size and line height bold style -->
										<h3 class="tbk__title montserrat fs-38 lh-46 fw-bold">TRULY MULTI-PURPOSE <span class="text-custom fw-normal tcolor">&amp;</span> OUTSTANDING</h3>
										<!--/ Title with montserrat font, custom size and line height bold style -->

										<!-- symbol -->
										<span class="tbk__symbol "><span></span></span>
										<!--/ symbol -->

										<!-- Sub-Title -->
										<h4 class="tbk__subtitle fs-18 lh-32 fw-vthin">Dramatically expedite emerging ROI through diverse deliverables. Phosfluorescently integrate covalent portals without cooperative e-services.</h4>
										<!--/ Sub-Title -->
									</div>
									<!--/ Title element -->

									<!-- separator -->
									<div class="hg_separator style2 clearfix">
									</div>
									<!--/ separator -->
								</div>
								<!--/ col-md-12 col-sm-12 -->

								<div class="col-md-8 col-sm-8">
									<!-- Text box -->
									<div class="text_box">
										<!-- Description -->
										<p>
											Credibly build out-of-the-box functionalities before strategic expertise. Competently reconceptualize resource maximizing relationships via business synergy.
										</p>
										<p>
											Initiate user friendly content with low-risk high-yield human capital.&nbsp;Compellingly redefine 2.0 services via fully tested experiences. Monotonectally plagiarize market-driven alignments for team building.
										</p>
										<!--/ Description -->
									</div>
									<!-- Text box -->
								</div>
								<!--/ col-md-8 col-sm-8 -->

								<div class="col-md-4 col-sm-4">
									<!-- Video box pop-up (adbox) element -->
									<div class="adbox video">
										<!-- Image -->
										<img src="images/sleepy1_1x-400x300.jpg" alt="" title="" />
										<!-- Image -->

										<!-- Video trigger wrapper -->
										<div class="video_trigger_wrapper">
											<!-- Video box container -->
											<div class="adbox_container">
												<!-- Video link -->
												<a class="playVideo" data-lightbox="iframe" href="https://www.youtube.com/watch?v=cVt-3vbENOQ"></a>
											</div>
											<!--/ Video trigger container -->
										</div>
										<!--/ Video box wrapper -->
									</div>
									<!--/ Video box pop-up (adbox) element -->
								</div>
								<!--/ col-md-4 col-sm-4 -->
							</div>
							<!--/ row hg_col_eq_last -->
						</div>
					</div>
					<!--/ col-md-7 col-sm-12 -->
				</div>
				<!--/ row gutter-lg -->
			</div>
			<!--/ full_width -->
		</section>
		<!--/ Media Container - Border Animate Style 2 section with background white color -->

		<!-- Hover Boxes section whith custom top padding -->
		<section class="hg_section ptop-65">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<!-- Hover box element -->
						<div>
							<!-- Link box with background color -->
							<a href="#" target="_blank" style="background-color: #34495e;" class="hover-box hover-box-2">
								<!-- Hover icon -->
								<span class="hb-circle"></span>
								<!--/ Hover icon -->

								<!-- Image/Icon -->
								<img src="images/quote1.svg" class="hb-img rb-right" alt="" title="" />
								<!--/ Image/Icon -->

								<!-- Title -->
								<h3>get a quote</h3>
								<!--/ Title -->

								<!-- Description -->
								<p>
									Share your idea and let’s create<br>
									a great project together!
								</p>
								<!--/ Description -->
							</a>
							<!--/ Link box with background color -->
						</div>
						<!--/ Hover box element -->
					</div>
					<!--/ col-md-4 col-sm-4 -->

					<div class="col-md-4 col-sm-4">
						<!-- Hover box element -->
						<div>
							<!-- Link box with background color-->
							<a href="#" target="_blank" style="background-color: #34495e;" class="hover-box hover-box-2">
								<!-- Hover icon -->
								<span class="hb-circle"></span>
								<!-- Hover icon -->

								<!-- Image/Icon -->
								<img src="images/hb-hiring.svg" class="hb-img rb-right" alt="" title="" />
								<!--/ Image/Icon -->

								<!-- Title -->
								<h3>hiring: ON</h3>
								<!--/ Title -->

								<!-- Subtitle -->
								<h4>Developer wanted! </h4>
								<!-- Subtitle -->

								<!-- Description -->
								<p>
									Send your kickass resume<br>
									 on jobs@mywebsite.com
								</p>
								<!--/ Description -->
							</a>
							<!--/ Link box with background color -->
						</div>
						<!--/ Hover box element -->
					</div>
					<!--/ col-md-4 col-sm-4 -->

					<div class="col-md-4 col-sm-4">
						<!-- Hover box element -->
						<div>
							<!-- Link box with background image -->
							<a href="#" target="_blank" style="background-image:url(images/hb-catalogue1.png); background-color: #34495e; background-size:cover" class="hover-box hover-box-3">
								<!-- Image/Icon -->
								<img src="images/hb-catalogue1.png" class="hb-img " alt="" title="" />
								<!--/ Image/Icon -->

								<!-- Title -->
								<h3>download catalogue</h3>
								<!--/ Title -->

								<!-- Description -->
								<p>
									Read our latest catalog, collateral material or print advertisements for your publication.
								</p>
								<!--/ Description -->
							</a>
							<!--/ Link box with background image -->
						</div>
						<!--/ Hover box element -->
					</div>
					<!--/ col-md-4 col-sm-4 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Hover Boxes section whith custom top padding -->

		<!-- Latest Posts - Accordion Style section -->
		<section class="hg_section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Latest posts accordion style -->
						<div class="latest_posts acc-style">
							<!-- Title -->
							<h3 class="m_title">OUR LATEST STORIES</h3>
							<!--/ Title -->

							<!-- View all posts button -->
							<a href="#" class="viewall">VIEW ALL -</a>
							<!--/ View all posts button -->

							<!-- CSS3 Accordion -->
							<div class="css3accordion">
								<ul>
									<!-- Post -->
									<li>
										<!-- Post wrapper -->
										<div class="inner-acc" style="width: 570px;">
											<!-- Post link wrapper -->
											<a href="blog-post.html" class="thumb hoverBorder plus">
												<!-- Border wrapper -->
												<span class="hoverBorderWrapper">
													<!-- Image -->
													<img src="images/css3panels-alt-04-370x200.jpg" alt="" title="" />
													<!--/ Image -->

													<!-- Hover border/shadow -->
													<span class="theHoverBorder"></span>
													<!--/ Hover border/shadow -->
												</span>
												<!--/ Border wrapper -->
											</a>
											<!-- Post link wrapper -->

											<!-- Post content -->
											<div class="content">
												<!-- Details & tags -->
												<em>21 August 2017 by danut, in Mobile</em>
												<!--/ Details & tags -->

												<!-- Title with link -->
												<h5 class="m_title"><a href="blog-post.html">Enthusiastically administrate ubiquitous</a></h5>
												<!--/ Title with link -->

												<!-- Content text -->
												<div class="text">
													Competently leverage other’s high standards in customer service after supe...
												</div>
												<!--/ Content text -->

												<!-- Read more button -->
												<a href="blog-post.html">READ MORE +</a>
												<!--/ Read more button -->
											</div>
											<!--/ Post content -->
										</div>
										<!--/ Post wrapper -->
									</li>
									<!--/ Post -->

									<!-- Post -->
									<li>
										<!-- Post wrapper -->
										<div class="inner-acc" style="width: 570px;">
											<!-- Post link wrapper -->
											<a href="blog-post.html" class="thumb hoverBorder plus">
												<!-- Border wrapper -->
												<span class="hoverBorderWrapper">
													<!-- Image -->
													<img src="images/general-bg-8-370x200.jpg" alt="" title="" />
													<!--/ Image -->

													<!-- Hover border/shadow -->
													<span class="theHoverBorder"></span>
													<!--/ Hover border/shadow -->
												</span>
												<!--/ Border wrapper -->
											</a>
											<!--/ Post link wrapper -->

											<!-- Post content -->
											<div class="content">
												<!-- Details & tags -->
												<em>07 August 2017 by Marius H., in Mobile,Technology</em>
												<!--/ Details & tags -->

												<!-- Title with link -->
												<h5 class="m_title"><a href="blog-post.html">Uniquely productize next-generation opportunities</a></h5>
												<!--/ Title with link -->

												<!-- Content text -->
												<div class="text">
													Appropriately pontificate synergistic para digms whereas 24/7 “outside the...
												</div>
												<!--/ Content text -->

												<!-- Read more button -->
												<a href="blog-post.html">READ MORE +</a>
												<!--/ Read more button -->
											</div>
											<!--/ Post content -->
										</div>
										<!--/ Post wrapper -->
									</li>
									<!--/ Post -->

									<!-- Post -->
									<li class="last">
										<!-- Post wrapper -->
										<div class="inner-acc" style="width: 570px;">
											<!-- Post link wrapper -->
											<a href="blog-post.html" class="thumb hoverBorder plus">
												<!-- Border wrapper -->
												<span class="hoverBorderWrapper">
													<!-- Image -->
													<img src="images/blog1-370x200.jpg" alt="" title="" />
													<!--/ Image -->

													<!-- Hover border/shadow -->
													<span class="theHoverBorder"></span>
													<!--/ Hover border/shadow -->
												</span>
												<!--/ Border wrapper -->
											</a>
											<!--/ Post link wrapper -->

											<!-- Post content -->
											<div class="content">
												<!-- Details & tags -->
												<em>07 August 2017 by Marius H., in Mobile,Networking</em>
												<!--/ Details & tags -->

												<!-- Title with link -->
												<h5 class="m_title"><a href="blog-post.html">Progressively repurpose cutting-edge models</a></h5>
												<!--/ Title with link -->

												<!-- Content text -->
												<div class="text">
													Seamlessly orchestrate process-centric best practices with end-to-end catalysts ...
												</div>
												<!--/ Content text -->

												<!-- Read more button -->
												<a href="blog-post.html">READ MORE +</a>
												<!--/ Read more button -->
											</div>
											<!--/ Post content -->
										</div>
										<!--/ Post wrapper -->
									</li>
									<!--/ Post -->
								</ul>
							</div>
							<!--/ CSS3 Accordion -->
						</div>
						<!--/ Latest posts accordion style -->
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ hg_section_size container -->
		</section>
		<!--/ Latest Posts - Accordion Style section -->

		<!-- Partners & Testimonials section with custom paddings -->
		<section class="hg_section hg_section--relative ptop-80 pbottom-80">
			<!-- Background -->
			<div class="kl-bg-source">
				<!-- Gradient overlay -->
				<div class="kl-bg-source__overlay" style="background:rgba(205,33,34,1); background: -moz-linear-gradient(left, rgba(205,33,34,1) 0%, rgba(245,72,76,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(205,33,34,1)), color-stop(100%,rgba(245,72,76,1))); background: -webkit-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: -o-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: -ms-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: linear-gradient(to right, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); ">
				</div>
				<!--/ Gradient overlay -->
			</div>
			<!--/ Background -->

			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Title element with bottom line style -->
						<div class="kl-title-block clearfix text-center tbk-symbol--line tbk-icon-pos--after-title">
							<!-- Title with montserrat font, white color and bold style -->
							<h3 class="tbk__title white montserrat fw-bold">BELOVED REVIEWS & TECHNOLOGIES</h3>
							<!--/ Title with montserrat font, white color and bold style -->

							<!-- Title bottom symbol -->
							<div class="tbk__symbol ">
								<span></span>
							</div>
							<!--/ Title bottom symbol -->
						</div>
						<!--/ Title element with bottom line style -->

						<!-- Testimonials & Partners element - light text style -->
						<div class="testimonials-partners testimonials-partners--light">
							<!-- Testimonials  element-->
							<div class="ts-pt-testimonials clearfix">
								<!-- Item - size 2 and normal style with custom margin top -->
								<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-2 ts-pt-testimonials__item--normal" style="margin-top:20px; ">
									<!-- Testimonial text -->
									<div class="ts-pt-testimonials__text">
										“Credibly innovate granular internal or "organic" sources whereas high standards in web readiness. Energistically scale future-proof core competencies vis-a-vis impactful experiences. Dramatically synthesize integrated schemas with.
									</div>
									<!--/ Testimonial text -->

									<!-- Testimonial info -->
									<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
										<!-- User image -->
										<div class="ts-pt-testimonials__img" style="background-image:url('images/t2.jpg');" title="JIMMY FERRARA">
										</div>
										<!--/ User image -->

										<!-- Name -->
										<h4 class="ts-pt-testimonials__name">JIMMY FERRARA</h4>
										<!--/ Name -->

										<!-- Position -->
										<div class="ts-pt-testimonials__position">
											GENERAL MANAGER
										</div>
										<!--/ Position -->

										<!-- Review stars - 5 stars -->
										<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--5">
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
										</div>
										<!--/ Review stars - 5 stars -->
									</div>
									<!--/ Testimonial info - .ts-pt-testimonials__infos -->
								</div>
								<!--/ Item - size 2 and normal style with custom margin top - .ts-pt-testimonials__item -->

								<!-- Item - size 1 and normal style -->
								<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-1 ts-pt-testimonials__item--normal" style=" ">
									<!-- Testimonial text -->
									<div class="ts-pt-testimonials__text">
										“Credibly innovate granular internal or "organic" sources whereas high standards in web readiness.
									</div>
									<!--/ Testimonial text -->

									<!-- Testimonials info -->
									<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
										<!-- User image -->
										<div class="ts-pt-testimonials__img" style="background-image:url('images/t1.jpg');" title="PERRY ANDREWS">
										</div>
										<!--/ User image -->

										<!-- Name -->
										<h4 class="ts-pt-testimonials__name">PERRY ANDREWS</h4>
										<!--/ Name -->

										<!-- Position -->
										<div class="ts-pt-testimonials__position">
											SWIFT Inc.
										</div>
										<!--/ Position -->

										<!--/ Review stars - 4 stars -->
										<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--4">
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
										</div>
										<!--/ Review stars - 4 stars -->
									</div>
									<!--/ Testimonials info - .ts-pt-testimonials__infos -->
								</div>
								<!--/ Item - size 1 and normal style - .ts-pt-testimonials__item -->

								<!-- Item - size 1 and reversed style -->
								<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-1 ts-pt-testimonials__item--reversed" style=" ">
									<!-- Testimonial info -->
									<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
										<!-- User image -->
										<div class="ts-pt-testimonials__img" style="background-image:url('images/t3.jpg');" title="SAMMY BROWNS">
										</div>
										<!--/ User image -->

										<!-- Name -->
										<h4 class="ts-pt-testimonials__name">SAMMY BROWNS</h4>
										<!--/ Name -->

										<!-- Position -->
										<div class="ts-pt-testimonials__position">
											CFO, Perfect Inc.
										</div>
										<!--/ Position -->

										<!-- Review stars - 5 stars -->
										<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--5">
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
											<span class="glyphicon glyphicon-star"></span>
										</div>
										<!--/ Review stars - 5 stars -->
									</div>
									<!--/ Testimonial info - .ts-pt-testimonials__infos -->

									<!-- Testimonial text -->
									<div class="ts-pt-testimonials__text">
										“Credibly innovate granular internal or "organic" sources whereas high standards in web readiness.
									</div>
									<!--/ Testimonial text -->
								</div>
								<!--/ Item - size 1 and reversed style - .ts-pt-testimonials__item -->
							</div>
							<!--/ Testimonials element - .ts-pt-testimonials-->

							<!-- Separator for testimonials-partners elements -->
							<div class="testimonials-partners__separator clearfix">
							</div>
							<!--/ Separator for testimonials-partners elements -->

							<!-- Partners element -->
							<div class="ts-pt-partners ts-pt-partners--y-title clearfix">
								<!-- Title -->
								<div class="ts-pt-partners__title">
									TECHNOLOGIES
								</div>
								<!--/ Title -->

								<!-- Partners carousel wrapper -->
								<div class="ts-pt-partners__carousel-wrapper">
									<div class="ts-pt-partners__carousel">
										<!-- Item #1 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo6.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #1 -->

										<!-- Item #2 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo7.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #2 -->

										<!-- Item #3 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo8.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #3 -->

										<!-- Item #4 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo1.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #4 -->

										<!-- Item #5 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo2.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #5 -->

										<!-- Item #6 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo3.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #6 -->

										<!-- Item #7 -->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo4.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #7 -->

										<!-- Item  #8-->
										<div class="ts-pt-partners__carousel-item">
											<!-- Partner image link -->
											<a class="ts-pt-partners__link" href="#" target="_self" title="">
												<!-- Image -->
												<img class="ts-pt-partners__img" src="images/logo5.svg" alt="" title="" />
												<!--/ Image -->
											</a>
											<!--/ Partner image link -->
										</div>
										<!--/ Item #8 -->
									</div>
									<!--/ .ts-pt-partners__carousel -->
								</div>
								<!--/ Partners carousel wrapper -->
							</div>
							<!--/ Partners element - .ts-pt-partners -->
						</div>
						<!--/ Testimonials & Partners element - light text style -->
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Partners & Testimonials section with custom paddings -->

@endsection