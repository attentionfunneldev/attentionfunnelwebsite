@extends('layouts.base')

@section('content')
    
    @section('subheader-background-image') /images/about-me.jpg @endsection
    @section('page-title') CONTACT US @endsection
    @section('subtitle') CONNECT WITH OUR TEAM @endsection
    @section('description') At AttentionFunnel we leverage social culture to elevate your brand and online presence. 
                                        Join us as we guide you into the forefront of the digital frontier.
    @endsection
    
	@include('subheaders.subheader')

		<!-- Progress bars skill section -->
		<section class="hg_section pt-70 pb-120">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="skills_wgt stg-rounded mt-sm-50" data-animated="execute">
							<!-- Title -->
							<h2 class="wgt-title"><span>My skills</span></h2>										
							<ul>
								<li>
									<h5 class="skill-title">Wordpress</h5>
									<span class="skill-bar" data-color="t-red" data-loaded="80"><i></i></span>
								</li>
								<li>
									<h5 class="skill-title">HTML &amp; CSS</h5>
									<span class="skill-bar" data-color="t-red" data-loaded="90"><i></i></span>
								</li>
								<li>
									<h5 class="skill-title">PHP</h5>
									<span class="skill-bar" data-color="t-red" data-loaded="70"><i></i></span>
								</li>
								<li>
									<h5 class="skill-title">JavaScript</h5>
									<span class="skill-bar" data-color="t-red" data-loaded="50"><i></i></span>
								</li>
								<li>
									<h5 class="skill-title">Joomla</h5>
									<span class="skill-bar" data-color="t-red" data-loaded="80"><i></i></span>
								</li>
								<li>
									<h5 class="skill-title">Ruby On Rails</h5>
									<span class="skill-bar" data-color="t-red" data-loaded="40"><i></i></span>
								</li>
							</ul>
						</div>
					</div>
					<!--/ col-sm-6 -->

					<div class="col-sm-6">
						<h4 class="fw-semibold">Worked with</h4>
						<p>Pellentesque cursus arcu id magna euismod in elementum purus molestie. Curabitur pellentesque massa eu nulla consequat sed porttitor arcu porttitor. Quisque volutpat pharetra felis, eu cursus lorem molestie vitae. Nulla vehicula, lacus ut suscipit fermentum, turpis felis ultricies dui, ut rhoncus libero augue at libero. Morbi ut arcu dolor. Maecenas id nulla nec nibh viverra vehicula.</p>
						<p>Quisque volutpat pharetra felis, eu cursus lorem molestie vitae. Nulla vehicula.</p>
						<ul class="logolist mt-30">
							<li><a href="#"><img src="images/clients/clients2.png" alt=""></a></li>
							<li><a href="#"><img src="images/clients/clients3.png" alt=""></a></li>
							<li><a href="#"><img src="images/clients/clients1.png" alt=""></a></li>
							<li><a href="#"><img src="images/clients/clients4.png" alt=""></a></li>
							<li><a href="#"><img src="images/clients/clients5.png" alt=""></a></li>
						</ul>
					</div>
					<!--/ col-sm-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Progress bars skill section -->

		<!-- Numbers counting section -->
		<section class="hg_section bg-lightgray pt-100 pb-80">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Counter element -->
						<div class="justified-blocks">
							<div class="fun-fact jsf-block">
								<span class="kl-icon tcolor icon-gi-ico-14 fs-32"></span>
								<span class="fun-number fw-thin counter">520</span>
								<span class="fun-thing topline fw-bold mt-40">MARKETING STRATEGY</span>
							</div>
							<div class="fun-fact jsf-block">
								<span class="kl-icon tcolor icon-gi-ico-5 fs-32"></span>
								<span class="fun-number fw-thin counter">999</span>
								<span class="fun-thing topline fw-bold mt-40">NEW PROJECT IDEAS</span>
							</div>
							<div class="fun-fact jsf-block">
								<span class="kl-icon tcolor icon-process3 fs-32"></span>
								<span class="fun-number fw-thin counter">180</span>
								<span class="fun-thing topline fw-bold mt-40">DONE PROJECTS</span>
							</div>
							<div class="fun-fact jsf-block">
								<span class="kl-icon tcolor icon-gi-ico-6 fs-32"></span>
								<span class="fun-number fw-thin counter">152</span>
								<span class="fun-thing topline fw-bold mt-40">HAPPY CUSTOMERS</span>
							</div>
							<div class="jsf-stretch"></div>
						</div>
						<!--/ Counter element -->
					</div>
					<!--/ col-md-12 col-sm-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Numbers counting section -->


@endsection