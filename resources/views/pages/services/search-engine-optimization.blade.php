@extends('layouts.base')

@section('content')
    
    @section('subheader-background-image') /images/about-me.jpg @endsection
    @section('page-title') SEARCH ENGINE OPTIMIZATION @endsection
    @section('subtitle') IT'S NOT A PROMISE BUT A BEST PRACTICE @endsection
    @section('description') Lorem ipsum sit dolor amet consectur lorem ipsum sit dolor amet consectur.
    Lorem ipsum sit dolor amet consectur lorem ipsum sit dolor amet consectur.
    @endsection
    
    @include('subheaders.subheader')
    
@endsection