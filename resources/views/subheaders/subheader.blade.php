<!-- Page Sub-Header + mask style 6 -->
<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask6">
			<div class="bgback"></div>

			<!-- Bakcground -->
			<div class="kl-bg-source">
				<!-- Background image -->
				<div class="kl-bg-source__bgimage" style="background-image:url(@yield('subheader-background-image')); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover">
				</div>
				<!--/ Background image -->

				<!-- Gradient overlay -->
				<div class="kl-bg-source__overlay" style="">
				</div>
				<!--/ Gradient overlay -->
			</div>
			<!--/ Bakcground -->

			<!-- Animated Sparkles -->
			<div class="th-sparkles"></div>
			<!--/ Animated Sparkles -->

			<!-- Sub-Header content wrapper with custom min height -->
			<div class="ph-content-wrap h-600">
				<div class="ph-content-v-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">

							</div>
							<!--/ col-sm-6 -->

							<div class="col-sm-6 col-sm-offset-6">
								<!-- Sub-Header Titles -left aligned -->
								<div class="subheader-titles left">
									<!-- Main Title -->
									<h2 class="subheader-maintitle fs-xxxxl fs-sm-xxl fw-extrabold reset-line-height mb-0 mt-xs-70">@yield('page-title')</h2>
									<!--/ Main Title -->

									<!-- Main Sub-Title with template color and margn bottom -->
									<h4 class="subheader-subtitle fw-semibold tcolor mb-20">@yield('subtitle')</h4>
									<!--/ Main Sub-Title wth template color and margin bottom -->

									<!-- Description -->
									<p class="gray">@yield('description')</p>

									<!-- Social -->
									<ul class="social_icons socialstyles-extra mt-30">
										<li><a href="#" class="kl-icon icon-linkedin"></a></li>
										<li><a href="#" class="kl-icon icon-twitter"></a></li>
									</ul>
								</div>
								<!--/ Sub-Header Titles - left aligned-->
							</div>
							<!--/ col-sm-6 -->
						</div>
						<!--/ row -->
					</div>
					<!--/ container -->
				</div>
				<!--/ ph-content-v-center -->
			</div>
			<!--/ Sub-Header content wrapper with custom min height -->

			<!-- Bottom mask style 7 -->
			<div class="kl-bottommask kl-bottommask--mask7">
				<svg width="767px" height="60px" class="kl-bottommask--mask7 screffect opacity3" viewBox="0 0 767 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none">
					<polygon fill="#000" points="767 0 767 60 0 60 "></polygon>
				</svg>
				<svg width="767px" height="50px" class="kl-bottommask--mask7 mask-over" viewBox="0 0 767 50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none">
					<polygon fill="#f5f5f5" points="767 0 767 50 0 50 "></polygon>
				</svg>
			</div>
			<!--/ Bottom mask style 7 -->

		
		</div>
		<!--/ Page Sub-Header + mask style 6 -->
