<!-- main menu -->
<div id="main-menu" class="main-nav ">
    <ul id="menu-main-menu" class="main-menu ">
        <li class="menu-item"><a href="/about">ABOUT</a></li>
        <li class="menu-item"><a href="/case-study">CASE STUDY</a></li>
        <li class="menu-item"><a href="/news">NEWS</a></li>
        <li class="menu-item"><a href="/services">SERVICES</a></li>
        <li class="menu-item"><a href="/resources">RESOURCES</a></li>
        <li class="menu-item"><a href="/contact">CONTACT</a></li>
    </ul>
</div>
<!--/ main menu -->