    	<!-- Title with Call to Action Button section with custom paddings -->
		<section class="hg_section bg-white ptop-65 pb-20">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-9">
						<!-- Title element -->
						<div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
							<!-- Title -->
							<h3 class="tbk__title montserrat fw-semibold tcolor">WORK WITH US</h3>
							<!--/ Title -->

							<!-- Sub-Title -->
							<h4 class="tbk__subtitle fw-thin">We'll do everything we can to make our next best project!</h4>
							<!--/ Sub-Title -->
						</div>
						<!--/ Title element -->
					</div>
					<!--/ col-md-9 col-sm-9 -->

					<div class="col-md-3 col-sm-3">
						<!-- spacer with custom height -->
						<div class="th-spacer clearfix" style="height: 10px;">
						</div>
						<!--/ spacer with custom height -->

						<!-- Button lined style -->
						<div class="text-left">
							<a class="btn-element btn btn-lined lined-custom btn-md btn-block " href="#" style="margin:0 0 10px 0;">
								<span>GET A QUOTE</span>
							</a>
						</div>
						<!--/ Button lined style -->
					</div>
					<!--/ col-md-3 col-sm-3 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!-- Title with Call to Action Button section with custom paddings -->
    <!-- Footer - Default Style -->
    <footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-5">
						<div>
							<h3 class="title m_title">FOOTER MENU</h3>
							<div class="sbs">
								<ul class="menu">
									<li><a href="#">Curabitur iaculis</a></li>
									<li><a href="#">Parturient montes</a></li>
									<li><a href="#">Vulputate magna</a></li>
									<li><a href="#">Cum sociis natoque</a></li>
									<li><a href="#">Nulla varius commodo</a></li>
									<li><a href="#">Parturient montes</a></li>
									<li><a href="#">Vulputate magna</a></li>
									<li><a href="#">Cum sociis natoque</a></li>
									<li><a href="#">Nulla varius commodo</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!--/ col-sm-5 -->

					<div class="col-sm-4">
						<div class="newsletter-signup">
							<h3 class="title m_title">NEWSLETTER SIGNUP</h3>
							<p>By subscribing to our mailing list you will always be update with the latest news from us.</p>
							<form action="http://YOUR_USERNAME.DATASERVER.list-manage.com/subscribe/post-json?u=YOUR_API_KEY&amp;id=LIST_ID&c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<input type="email" value="" name="EMAIL" class="nl-email form-control" id="mce-EMAIL" placeholder="your.address@email.com" required>
		                        <input type="submit" name="subscribe" class="nl-submit" id="mc-embedded-subscribe" value="JOIN US">
		                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		                        <div style="position: absolute; left: -5000px;">
		                            <input type="text" name="b_xxxxxxxxxxxxxxxxxxxCUSTOMxxxxxxxxx" value="">
		                        </div>
							</form>	
							<div id="notification_container"></div>
							<p>We never spam!</p>
						</div><!-- end newsletter-signup -->
					</div>
					<!-- col-sm-4 -->

					<div class="col-sm-3">
						<div>
							<h3 class="title m_title">GET IN TOUCH</h3>
							<div class="contact-details"><p><strong>T (212) 555 55 00</strong><br>
								Email: <a href="#">sales@yourwebsite.com</a></p>
								<p>Your Company LTD<br>
								Street nr 100, 4536534, Chicago, US</p>
								<p><a href="http://goo.gl/maps/1OhOu" target="_blank"><i class="icon-map-marker icon-white"></i> Open in Google Maps</a></p>
							</div>
						</div>
					</div>
					<!--/ col-sm-3 -->
				</div>
				<!--/ row -->

				<div class="row">
					<div class="col-sm-6">
						<div class="twitterFeed">				
							<!-- twitter feeds -->
							<div class="tweets" id="twitterFeed"><small>Please wait whilst our latest tweets load</small></div>
							<a href="https://twitter.com/hogash" class="twitter-follow-button" data-show-count="false">Follow @hogash</a>
							<!-- twitter script -->
							<script>
								! function(d, s, id) {
									var js, fjs = d.getElementsByTagName(s)[0];
									if (!d.getElementById(id)) {
										js = d.createElement(s);
										js.id = id;
										js.src = "//platform.twitter.com/widgets.js";
										fjs.parentNode.insertBefore(js, fjs);
									}
								}(document, "script", "twitter-wjs");
							</script>
							<!--/ twitter script -->
						</div>
						<!--/ twitter-feed -->
					</div>
					<!--/ col-sm-6 -->

					<div class="col-sm-6">
						<!-- social-share buttons -->
						<div class="social-share">
							<!-- Facebook button -->
							<div class="fb-like" data-href="https://www.facebook.com/hogash.themeforest" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
							<!--/ Facebook button -->
							<!-- Facebook Script -->
							<div id="fb-root"></div>
							<script>
								(function(d, s, id) {
									var js, fjs = d.getElementsByTagName(s)[0];
									if (d.getElementById(id)) return;
									js = d.createElement(s); js.id = id;
									js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=1380815252226236";
									fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));
							</script>
							<!--/ Facebook Script -->

							<!-- Twitter button -->
							<div class="twitter-button">
								<!-- Change data-via to your own. -->
								<a href="https://twitter.com/share" class="twitter-share-button" data-via="hogash" data-counturl="https://dev.twitter.com/web/tweet-button">Tweet</a>
							</div>
							<!--/ Twitter button -->

							<!-- Google+ button -->
							<div class="google-button">
								<div class="g-follow" data-annotation="bubble" data-height="20" data-href="//plus.google.com/u/0/118411267887632617276" data-rel="author"></div>
								<script src="https://apis.google.com/js/platform.js" async defer></script>
							</div>
							<!--/ Google+ button -->

							<!-- Pinterest button -->
							<div class="pinterest-button">
								<a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonPin">
								</a>
								<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							<!--/ Pinterest button -->
						</div>
						<!--/ social-share -->
					</div>
					<!--/ col-sm-6 -->
				</div>
				<!--/ row -->

				<div class="row">
					<div class="col-sm-12">
						<div class="bottom clearfix">
							<!-- social-icons -->
							<ul class="social-icons sc--clean clearfix">
								<li class="title">GET SOCIAL</li>
								<li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
								<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>	
								<li><a href="#" target="_self" class="icon-dribbble" title="Dribbble"></a></li>
								<li><a href="#" target="_blank" class="icon-google" title="Google Plus"></a></li>
							</ul>
							<!--/ social-icons -->

							<!-- copyright -->
							<div class="copyright">
								<p>© 2017 All rights reserved.</p>
							</div>
							<!--/ copyright -->
						</div>
						<!--/ bottom -->
					</div>
					<!--/ col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
        </footer>
        
	


	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/kl-plugins.js"></script>

	<!-- JS FILES // Loaded on this page -->
	<!-- Requried js script for Slideshow Scroll effect -->
	<script type="text/javascript" src="/js/plugins/scrollme/jquery.scrollme.js"></script>

	<!-- Required js script for iOS Slider -->
	<script type="text/javascript" src="/js/plugins/_sliders/ios/jquery.iosslider.min.js"></script>

	<!-- Required js trigger for iOS Slider -->
	<script type="text/javascript" src="/js/trigger/slider/ios/kl-ios-slider.js"></script>

	<!-- CarouFredSel - Required js script for Screenshot box / Partners Carousel -->
	<script type="text/javascript" src="/js/plugins/_sliders/caroufredsel/jquery.carouFredSel-packed.js"></script>

	<!-- Required js trigger for Screenshot Box Carousel -->
	<script type="text/javascript" src="/js/trigger/kl-screenshot-box.js"></script>

	<!-- Required js trigger for Partners Carousel -->
	<script type="text/javascript" src="/js/trigger/kl-partners-carousel.js"></script>	

	<!-- Custom Kallyas JS codes -->
	<script type="text/javascript" src="/js/kl-scripts.js"></script>

	<!-- Custom user JS codes -->
	<script type="text/javascript" src="/js/kl-custom.js"></script>

	
		<!--/ Footer - Default Style -->