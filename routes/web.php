<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    // return view('pages.index');
    if(env('APP_ENV')=='production'){
        return view('pages.coming-soon');
    }else{
        return view('pages.index');
    }
});

Route::post('/subscribe','EmailSubscriberController@create');

Route::get('/services', function(){
    return view('pages.services');
});
Route::get('/about',function(){
    return view('pages.about');
});
Route::get('/contact',function(){
    return view('pages.contact');
});
Route::get('/resources',function(){
    return view('pages.resources');
});
Route::get('/news',function(){
    return view('pages.news');
});
Route::get('/case-study',function(){
    return view('pages.case-study');
});

Route::prefix('services')->group(function () {
    Route::get('branding', function () { return view('pages.services.branding'); });
    Route::get('creative-production', function () { return view('pages.services.creative-production'); });
    Route::get('digital-marketing', function () { return view('pages.services.digital-marketing'); });
    Route::get('seo', function () { return view('pages.services.search-engine-optimization'); });
    Route::get('social-media', function () { return view('pages.services.social-media'); });
    Route::get('web-development', function () { return view('pages.services.web-development'); });
    Route::get('workshops', function () { return view('pages.services.workshops'); });
});